---
title: "Loslegen"
linkTitle: "Loslegen"
weight: 2
description: >
  Wie man den Quellcode herunterläd und kompiliert
---

## Systemvorraussetzungen

- Ein Debian oder Fedora System. Andere GNU/Linux basierte Systeme funktionieren vielleicht, abe wir haben sie nicht getestet.
- Extrem viel Spieicherplatz. 200GB könnten gut sein.
- Extrem viel CPU-Power und viele Kerne.
- Viel RAM. Unter 8GB wird auf keinen Fall empfohlen, wir empfehlen midestens 16GB (auch Swap möglich)
- Wenn du mit einer -j-Option kompilierst, stelle sicher, dass du mindestens **3.2*Prozesse GB RAM** hast, ansonsten stürzt die Kompilation von framework.jar ab, aber der Buildprozess wird nicht beendet

## Repo tool

Androids Quellcode ist sehr komplex, aber `repo` kann damit umgehen. Lade es herunter:
```bash
mkdir -p ~/bin
echo 'export PATH=~/bin:$PATH' >> ~/.bashrc
source ~/.bashrc

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

git config --global user.name "Dein Name"
git config --global user.email "du@beispiel.de"
```
## AOPS-Quellode

### Das Projekt initialisieren

Dieses Beispiel läd die Android-Version `android-10.0.0_r33` von Google herunter. Die jeweils neuste Version findest du auf der
[ofiziellen Android-Webseite](https://source.android.com/setup/start/build-numbers#source-code-tags-and-builds).

```bash
repo init                                                  \
     -u https://android.googlesource.com/platform/manifest \
     -b android-10.0.0_r33                                 \
     --current-branch                                      \
     --no-tags                                             \
     --no-clone-bundle                                     \
     --depth 1                                             \
     -j$(nproc)
```

Für LineageOS, verwende diese URL mit dem branch `lineage-17.1`:

```
git://github.com/LineageOS/android.git
```

### Raspberry Pi Manifest

Damit der Raspberry Pi von Android unterstützt wird, wird noch einiges benötigt. Mit diesem Befehl wird repo angewiesen, auch alles für den Raspberry Pi herunterzuladen.

```bash
git clone \
  https://gitlab.com/rpi-droid/local_manifests.git \
  .repo/local_manifests                            \
  -b android10
```

Information: Dies ist lädt aalles für RPi 2 (Rev. 1.2), 3 und 4 herunter. Wenn du nur für den Raspberry Pi 2 herunterladen möchtest, verwende stattdessen`https://gitlab.com/rpi-droid/rpi2/local_manifests.git` als URL.
Das Selbe gilt für Pi 3 & 4.

### Alles herunterladen

```bash
repo sync              \
     --current-branch  \
     --no-tags         \
     --no-clone-bundle \
     -j$(nproc)
```

## Kompilieren

### Den Kernel kompilieren

```bash
cd kernel/rpi
ARCH=arm64 make rpi4droid_defconfig

ARCH=arm64 CROSS_COMPILE=aaarch64-linux-gnu- \
           make Image dtbs -j$(nproc)
```

### Android kompilieren

```bash
# make clean -j$(nproc) # if build fails, start clean.

source build/envsetup.sh

lunch # you'll have to choose your RPi then.

make -j$(nproc) ramdisk systemimage vendorimage
```

## Ausprobieren!

### Die Speicherkarte vorbereiten

Die Speicherkarte solte so formatiert werden:

| Partionsnummer |   Größe    |   Name   | Info                                    	         |
|----------------|------------|----------|-------------------------------------------------------|
| p1             | 256MB      | BOOT     | Verwende fdisk : W95 FAT32(LBA) & Bootable, mkfs.vfat |
| p2             | 1GB/1024MB | system   | Verwende fdisk, new primary partition                 |
| p3             | 128MB      | vendor   | Verwende fdisk, new primary partition                 |
| p4             | Space left | userdata | Verwende fdisk, mkfs.ext4                             |

Verwende die `-L`-Option von mkfs.ext4, den `e2label`-Befehl, die `-n`-Option von mkfs.vfat
oder [gparted](https://gparted.org/).

**WARNUNG** Wenn diese Reihenfolge nicht verwendet wird, kann Android nicht starten.

### Write _system_ and _vendor_ partition

Führe diese Befehle aus:

cd out/target/product/*dein Pi*
sudo dd if=system.img of=/dev/BLOCK-DEVICE bs=1M
sudo dd if=vendor.img of=/dev/BLOCK-DEVICE bs=1M

**SEI SEHR VORSICHTIG** ein Fehler beim Auswählen des korrekten _BLOCK-DEVICE_
könnte all deine Daten löschen. Wenn du eine Speicherkarte verwendest,
_könnte_ der Pfad in der folgenden Liste sein (muss aber nicht, pass also auf):

| /dev/mmcblk0p1 /dev/mmcblk0p2 ...

Wenn du den Befehl `dmesg -w` ausführst und danach die Speicherkarte einführstm sollte der der Speicherkarte zugewiesene Name im Terminal erscheinen.

**WIR ÜBERNEHMEN KEINE HAFTUNG FÜR DATENVERLUST**

### _kernel_ & _ramdisk_ auf die _BOOT_-Partition kopieren

| Kopiere                                                     	| Nach                            |
|---------------------------------------------------------------|---------------------------------|
| device/brcm/*dein Pi*/boot/*                                 	| p1:/                            |
| kernel/rpi/arch/arm64/boot/Image                         	| p1:/                            |
| kernel/rpi/arch/arm64/boot/dts/broadcom/*		      	| p1:/                            |
| kernel/rpi/arch/arm64/boot/dts/overlays			| p1:/				  |
| out/target/product/*dein Pi*/ramdisk.img			| p1:/                            |
