---
title: "Übersicht"
linkTitle: "Übersicht"
weight: 1
description: >
  Eine Kurzübersicht über das Projekt
---


## Was ist dies?

Dies ist ein unfertiges Projekt mit dem Ziel, Android-Images für den Raspberry Pi zu erstellen. Weil wir noch nicht fertig sind, können wir noch keine kompilierten Images erstellen.

## Was kann das?


* **Wozu ist der Code gut?**: Android-Apps auf dem Raspberry Pi 2 (Rev 1.2) oder neuer auszuführen.

* **Wozu ist er nicht gut?**: Sachen, die mit Android nicht gehen, ältere Raspberry Pis.


## Was jetzt?


* [Loslegen](/getting-started/): Mit dem Kompilieren loslegen.

