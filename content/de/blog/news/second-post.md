---
title: "KMS-Treiber für den Raspberry Pi 4"
linkTitle: "KMS für den Raspberry Pi 4"
date: 2020-04-01
description: >
  Informationen über den neuen RPi4 KMS-Treiber und zukünftige Entwicklungen
---

# Der neue KMS-Treiber

Heute hat der Linux-Kernel 5.4 einen neuen KMS-Treiber erhalten (RPi-Repo, https://github.com/raspberrypi/linux/pull/3515#issuecomment-606830623).

## Was bedeutet das?

Wir können jetzt Mesa3D tatsächlich nutzen, um mehr Leistung zu erhalten.

## Mehr Informationen

Wir veröffentlichen blad neue Updates und unsere Webseite erhält vielleicht ein Redesign.
