
---
title: "Unser erster Blogpost"
linkTitle: "Erster Post"
date: 2020-01-19
description: >
  Dies ist unser erster Blogpost. Viel zu sagen gibt es nicht, aber so sieht unser Blog nicht so leer aus.
---

# Neues

## Unsere neue Webseite
Ab heute ist unsere neue Webseite online. Gefällt sie dir?

## Unsere Dokumentation und Build-Helfer

Dank mehr Unterstützung beim Projekt gibt es jetzt unsere [GitLab-Gruppe](https://gitlab.com/rpi-droid/rpi4), [zefies build helper](https://gitlab.com/rpi4_android/zefie_andropi_helper), eine gute Dokumentation, und viel mehr.
