---
title: "Ein großes Update"
linkTitle: "großes Update"
date: 2020-04-04
description: >
  Was wir heute getan haben
---

# Das neuste Update

Heute haben wir einige Änderungen am Projekt veröffentlicht. Dies ist eine Zusammenfassung der Änderungen.

## Der neue KMS-Treiber

Wie wir breits angekündigt haben, hat der Linux-Kernel 5.4 einen KMS-Treiber für den Raspberry Pi 4 erhalten (rpi repo, https://github.com/raspberrypi/linux/pull/3515#issuecomment-606830623).
Dieses neue Update verwendet nun diesen Kernel und KMS auf dem Raspberry Pi, sodass es nun weniger Probleme geben sollte.

## Vorbereitungen auf die finale Version

Jetzt können alle Build-Typen kompiliert werden (`eng`, `userdebug` und `user`). Die finale Version wird ein `user`-Build sein.

## Ist dies bereits getestet?

Nein, aber du könntest helfen, indem du selber kompiliesst und testest.

## Wie kann ich diese Version kompilieren?

Bitte folge der Anleitung, Zeifes Build-Tools funktionieren (noch) nicht mit dieser Version (liegt aber nicht an den Build-Tools).
