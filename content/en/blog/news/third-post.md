---
title: "Big update"
linkTitle: "Big update"
date: 2020-04-04
description: >
  What we did today
---

# The latest update

Today, we published some new updates to the project. This is a summary of what's been added.

## The new KMS driver

As we already announced, a new KMS driver for the Pi 4 was added to the Linux kernel 5.4 (rpi repo, https://github.com/raspberrypi/linux/pull/3515#issuecomment-606830623).
The new update now uses this kernel and KMS on rpi4, which will hopefully fix most of the issues.

## Preparations for final build

The repository now contains the option to build all 3 build types (`eng`, `userdebug` and `user`). The final build will be a `user`-build.

## Is this already tested?

No, but you can contribute by building and testing by yourself and submitting logfiles at our discord server.

## How can I build this?

Please follow the docs, zefie's build tools currently don't work with this version.
