---
title: "KMS driver for the Raspberry Pi 4"
linkTitle: "KMS driver for the Raspberry Pi 4"
date: 2020-04-01
description: >
  Informations about the new RPi4 KMS driver and the future development
---

# The new KMS driver

Today, a new KMS driver for the Pi 4 was added to the Linux kernel 5.4 (rpi repo, https://github.com/raspberrypi/linux/pull/3515#issuecomment-606830623).

## What does this mean?

We'll probably be able to enable mesa3d graphics in our builds. This allows much better graphics performance

## More informations

We'll launch some updates tomorrow. The documentation will also get some updates tommorow, we'll maybe also change the design of our website.