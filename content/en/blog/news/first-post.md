
---
title: "Our first blog post"
linkTitle: "First blog post"
date: 2020-01-19
description: >
  This is our first blog post. We don't have much to post yet, but this makes our blog look better.
---

# News

## Our new website
We launched our new website today. It looks much better. Do you like it?

## Our docs and build helpers

Much people supported the project, now we have a [GitLab group](https://gitlab.com/rpi-droid/rpi4), [zefies build helper](https://gitlab.com/rpi4_android/zefie_andropi_helper), good docs, and much more.
