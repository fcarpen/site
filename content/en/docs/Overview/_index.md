---
title: "Overview"
linkTitle: "Overview"
weight: 1
description: >
  Overview
---


## What is it?

This is an unfinished project about porting Android to the Raspberry Pi.
Since this is not finished, we can't provide compiled images, but we'll
upload one as soon as we get one working.

## Why do I want it?


* **What will it be good for?**: Running android apps on your Raspberry Pi 2 or later.

* **What will it be not good for?**: Things which don't work with android, older Raspberry Pis.


## Where should I go next?



* [Getting Started](/getting-started/): Get started with compiling this.

