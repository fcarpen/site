---
title: "Own device"
linkTitle: "Own device"
weight: 10
description: >
  How to add your own device to android
---

# Adding your own device to Android

This an unfinished guide about adding a custom device to android.

**Note:** This is not an official guide and is not always working.

## Getting started

You should have already downloaded the AOSP source code.
The first step is to create a folder for your device in the source tree.
The path should be something like device/_vendor_/_device_.
The first `device` is always the word "device", not the name of your actual device.
Then create the following files inside this folder:

- AndroidProducts.mk
- BoardConfig.mk
- _your device_.mk

## AndroidProducts.mk

Add the following code to AndroidProducts.mk:

```Makefile
PRODUCT_MAKEFILES := $(LOCAL_DIR)/YOURDEVICE.mk

COMMON_LUNCH_CHOICES := YOURDEVICE-eng \
			YOURDEVICE-userdebug \
			YOURDEVICE-user
```

Replace YOURDEVICE with the name of your device.

## BoardConfig.mk

This file tells android about the hardware of your device.

You define variables like this:

```Makefile
VariableName := Value 
```

You should set the following settings:

- `TARGET_ARCH`: The architecture of your device. Supported values are: `arm`, `x86`, `arm64` and `x86_64`.


## _your device_.mk

This tells Android the name of your device & which files/packages it will use. You should set the following settings (define variables like in BoardConfig.mk):

- `PRODUCT_NAME`: Name of the device
- `PRODUCT_DEVICE`: The board name
- `PRODUCT_BRAND`: The brand the device is customized for (if any, you can also add your own name here, but it will be visible in all builds)
- `PRODUCT_MODEL`: Model of the device
- `PRODUCT_MANUFACTURER`: The manufacturer of the device
- `PRODUCT_LOCALES`: A list of two-letter language code, two-letter country code pairs. These locales will be supported in the final image, the first locale is the default. Example: `en_GB, de_DE`


