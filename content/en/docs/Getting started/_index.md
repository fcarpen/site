---
title: "Getting Started"
linkTitle: "Getting Started"
weight: 2
description: >
  How to get the source and compile
---

## Prerequisites

- A Debian or Fedora based linux operating system. Other OSs may work but we
  have not tested them (yet).
- Lots and lots of free space. 200GB could be good.
- Lots and lots of CPU power and many cores.
- A lot of ram. Under 8GB is not recommended at all. Minimum 16GB is recommended.
- If you run with an -j option, make sure you have at least **3.2*jobs GB of RAM**, otherwise your framework.jar building will run out of ram, get killed, but not fail the build process!

## Repo tool

Android source tree can be overwhelmingly big and complex. `repo` is used to
manage and download it. Get it from google:

```bash
mkdir -p ~/bin
echo 'export PATH=~/bin:$PATH' >> ~/.bashrc
source ~/.bashrc

curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

git config --global user.name "Your Name"
git config --global user.email "you@example.com"
```
## AOSP Source

### Initialize The Project

This example fetches the android version `android-10.0.0_r33` from google. You
can always check the
[android official page](https://source.android.com/setup/start/build-numbers#source-code-tags-and-builds)
to find the latest tag.

```bash
repo init                                                  \
     -u https://android.googlesource.com/platform/manifest \
     -b android-10.0.0_r33                                 \
     --current-branch                                      \
     --no-tags                                             \
     --no-clone-bundle                                     \
     --depth 1                                             \
     -j$(nproc)
```

If you want to get Lineage source instead, in the above code snippet use the
following url with the branch `lineage-16.0`:

```
git://github.com/LineageOS/android.git
```

### Raspberry PI Manifest

Raspberry Pi has specific part uniqe to itself as a device. we need to tell the
repo to download these Raspberry Pi specific projects too:

```bash
git clone \
  https://gitlab.com/rpi-droid/local_manifests.git \
  .repo/local_manifests                            \
  -b android-10
```

Note: This is a generic manifest for RPi 2 (Rev. 1.2), 3 and 4. If you want to download only manifests for the Raspberry Pi 2, use `https://gitlab.com/rpi-droid/rpi2/local_manifests.git` instead.
Same applies for 3 & 4.

### Download the source

```bash
repo sync              \
     --current-branch  \
     --no-tags         \
     --no-clone-bundle \
     -j$(nproc)
```

## Build

### Build The Kernel

```bash
cd kernel/rpi
ARCH=arm64 make rpi4droid_defconfig

ARCH=arm64 CROSS_COMPILE=aaarch64-linux-gnu- \
           make Image dtbs -j$(nproc)
```

### Build Android

```bash
# make clean -j$(nproc) # if build fails, start clean.

source build/envsetup.sh

lunch # you'll have to choose your RPi then.

make -j$(nproc) ramdisk systemimage vendorimage
```

## Try it out!

### Prepare The SD Card

The partitions of the sd-card should be set-up like followings:

|  Partition No |  Size      |   Label  | Info                                            |
|---------------|------------|----------|-------------------------------------------------|
| p1            | 256MB      | BOOT     | Do fdisk : W95 FAT32(LBA) & Bootable, mkfs.vfat |
| p2            | 1GB/1024MB | system   | Do fdisk, new primary partition                 |
| p3            | 128MB      | vendor   | Do fdisk, new primary partition                 |
| p4            | Space left | userdata | Do fdisk, mkfs.ext4                             |

To label, use -L option of mkfs.ext4, e2label command, or -n option of mkfs.vfat
or use the application [gparted](https://gparted.org/).

**WARNING** You need to use this order, otherwise partitions can't be mounted correctly. 

### Write _system_ and _vendor_ partition

Run these commands:

cd out/target/product/rpi4
sudo dd if=system.img of=/dev/BLOCK-DEVICE bs=1M
sudo dd if=vendor.img of=/dev/BLOCK-DEVICE bs=1M

**BE VERY CAREFUL!** making a mistake in choosing the correct _BLOCK0-DEVICE_
you could erase all your data on your laptop or computer. If using an sd-card,
the path _could be_ in the following list (but could be not! make sure yourself):

| /dev/mmcblk0p1 /dev/mmcblk0p2 ...

You can run the command `dmesg -w`, and then insert the sdcard. The actual name
assigned to your sd-card will appear on terminal.

### Copy _kernel_ & _ramdisk_ to _BOOT_ partition

| Copy                                                     	| To                              |
|---------------------------------------------------------------|---------------------------------|
| device/brcm/*your_pi*/boot/*                                 	| p1:/                            |
| kernel/rpi/arch/arm64/boot/Image                         	| p1:/                            |
| kernel/rpi/arch/arm64/boot/dts/broadcom/*		      	| p1:/                            |
| kernel/rpi/arch/arm64/boot/dts/overlays			| p1:/				  |
| out/target/product/*your_pi*/ramdisk.img			| p1:/                            |
