#!/bin/bash 

set -eo pipefail

function docsy_usage() {
  cat <<EOF
  Usage:
    serve.sh -p   :   serve in production mode
    serve.sh -d   :   serve in development mode
    serve.sh -h   :   print this message
EOF
exit 0
}

function docsy_node_modules() {
  if ! [[ -e node_modules ]]; then
    npm install
  fi
}

function docsy_clean() {
  rm -rf public/
}

function docsy_dev() {
  docsy_node_modules
  hugo --environment development
}

function docsy_prod() {
  docsy_node_modules
  docsy_clean
  hugo --gc --minify --environment production
}

function docsy_serve() {
  hugo serve $@
}

function docsy_main() {
  case "$D_ARG" in
    d|dev|development|-d|--dev|--development)
      docsy_dev $@
      ;;

    p|prod|production|-p|--prod|--production)
      docsy_prod $@
      ;;

    s|serve|-s|--serve)
      docsy_serve $@
      ;;

   ''|def)
      docsy_prod $@
      docsy_serve --environment production $@
      ;;

    *)
      docsy_usage $@
      ;;
  esac
}


readonly D_ARG=$(echo "${1-}" | tr '[:upper:]' '[:lower:]')
if [[ $# -gt 0 ]]; then
  shift
fi

docsy_main $@

