# FAQ


## How can I update everything to a later version?

At first, cd into the folder with android.

Then run:

```
find ! -name .repo ! -name . -exec rm -rv {} \;
rm -r .repo/local_manifests
git clone https://github.com/RaspberryPiFan/local_manifests .repo/local_manifests -b android-10
repo sync --force-sync -j$(nproc)
```

This doesn't download everything again, it just downloads the latest updates and makes sure no file is broken.

## I can't build on WSL.

WSL is not working at this time, please use a real PC with an Debian GNU/Linux based OS or a VM.

## The generated images are stored inside a folder called "generic", not "rpi4".

Make sure you run these commands:
```
source build/envsetup.sh
lunch rpi4-eng
make ramdisk systemimage vendorimage -j$(nproc)
```
in one bash session.

## How can I rebuild Android, but not the kernel?

Run these commands:

```
make clean
source build/envsetup.sh
lunch rpi4-eng
make ramdisk systemimage vendorimage -j$(nproc)
```

## How can I rebuild the kernel, but not Android?

Run these commands:

```
cd kernel/rpi
ARCH=arm64 scripts/kconfig/merge_config.sh arch/arm64/configs/bcm2711_defconfig kernel/configs/android-base.config kernel/configs/android-recommended.config
ARCH=arm64 CROSS_COMPILE=aaarch64-linux-gnu- make Image dtbs -j$(nproc)
cd ../..
```

## How can I enable more debugging?

Use this as content of cmdline.txt:
```
console=serial0,115200 earlyprintk no_console_suspend debug root=/dev/ram0 elevator=deadline rootwait selinux=0 androidboot.hardware=rpi4 androidboot.selinux=0 buildvariant=eng androidboot.veritymode=disabled
```

## Why is it so hard to do this? Others already have ported android to the Raspberry Pi.

The developer who did this was a lot more experienced. Also our build will be made with mesa3d and be fully open source. We will also make our build for arm64 and based on android 10, lineage os coming later.
I think he will make better builds (his latest is not a final build) which include that, so our main goal is making an open source device configuration for the Raspberry Pi 4 which works.
The main problem with open source device config for the raspberry pi is that some people use it without a note about the original author and selling it as their own work.

## Where are your builds for other Pis?

We'll start working on them after a working pi 4 build.

## I'm getting other errors, how can I fix this?

This project is not finished yet, so this is normal. Please report the issue. (see [bug reporting](ISSUES.md) for more details).
