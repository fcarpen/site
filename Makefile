.PHONY: def serve build dev clean help

def:
	./serve.sh def

build:
	./serve.sh prod

dev:
	./serve.sh dev

serve:
	./serve.sh serve

help:
	./serve.sh help

clean:
	rm -rf public

